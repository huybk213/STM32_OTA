/*
 * Author: HuyTV7
 */

#ifndef GLOBAL_VAR_H
#define GLOBAL_VAR_H
#include "stdint.h"

#define USART2_RX_MAX_BUF_SIZE	1024

#ifndef	SER_RING_BUF_SIZE
#define SER_RING_BUF_SIZE			1024
#endif


#define SER_RING_BUF_RESET(serRingBuf)					(serRingBuf.rdIdx = serRingBuf.wrIdx = 0)
#define SER_RING_BUF_WR(serRingBuf, dataIn)			(serRingBuf.data[(SER_RING_BUF_SIZE - 1) & serRingBuf.wrIdx++] = (dataIn))
#define SER_RING_BUF_RD(serRingBuf)							(serRingBuf.data[(SER_RING_BUF_SIZE - 1) & serRingBuf.rdIdx++])
#define SER_RING_BUF_EMPTY(serRingBuf)					(serRingBuf.rdIdx == serRingBuf.wrIdx)
#define SER_RING_BUF_FULL(serRingBuf)						(serRingBuf.wrIdx == ((serRingBuf.rdIdx + SER_RING_BUF_SIZE) & 0x0FFFF))
#define SER_RING_BUF_RQ_WR(serRingBuf, dataIn)	(serRingBuf.data[(RING_BUF_SIZE - 1) & serRingBuf.wrIdx] = (dataIn))


typedef struct __SER_RING_BUF_T 
{
	uint8_t data[SER_RING_BUF_SIZE];
	uint16_t wrIdx;
	uint16_t rdIdx;
} SER_RING_BUF_T;



extern volatile uint32_t min_buf_index;
extern volatile uint8_t min_buffer[USART2_RX_MAX_BUF_SIZE];
#endif
