/*
* Author:	HuyTV7
 */
#include "min_hw_serial.h"

UART_HandleTypeDef MIN_UART_HANDLER;
SER_RING_BUF_T Ser_Min_Buf;

/* Hardware serial init function */
void MinHwSerial_Init(void)
{
	uint8_t tmp;
	/* Init clock */
	RCC_PeriphCLKInitTypeDef PeriphClkInit;
	__MIN_HW_GPIO_CLK_ENABLE();
	PeriphClkInit.PeriphClockSelection = MIN_HW_SERIAL_RCC;
  PeriphClkInit.Uart4ClockSelection = MIN_HW_SERIAL_CLK_SOURCE;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
	
	/* MSP Init*/
	GPIO_InitTypeDef GPIO_InitStruct;
  __MIN_HW_SERIAL_CLK_ENABLE();
  
   /*	USART GPIO Configuration */   
  GPIO_InitStruct.Pin = MIN_HW_SERIAL_TX_PIN | MIN_HW_SERIAL_RX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = MIN_HW_SERIAL_AF;
  HAL_GPIO_Init(MIN_HW_SERIAL_GPIO, &GPIO_InitStruct);

//   /* USART interrupt Init */
   HAL_NVIC_SetPriority(MIN_HW_SERIAL_IRQn, 0, 0);
   HAL_NVIC_EnableIRQ(MIN_HW_SERIAL_IRQn);

	/*Config UART	*/
	MIN_UART_HANDLER.Instance = MIN_UART_PORT;
  MIN_UART_HANDLER.Init.BaudRate = MIN_HW_SERIAL_BAUD;
  MIN_UART_HANDLER.Init.WordLength = UART_WORDLENGTH_8B;
  MIN_UART_HANDLER.Init.StopBits = UART_STOPBITS_1;
  MIN_UART_HANDLER.Init.Parity = UART_PARITY_NONE;
  MIN_UART_HANDLER.Init.Mode = UART_MODE_TX_RX;
  MIN_UART_HANDLER.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  MIN_UART_HANDLER.Init.OverSampling = UART_OVERSAMPLING_16;
  MIN_UART_HANDLER.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  MIN_UART_HANDLER.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  HAL_UART_Init(&MIN_UART_HANDLER);
	
	
	HAL_UART_Receive_IT(&MIN_UART_HANDLER, &tmp, 1);
	uint32_t x = 0x0000FFFF;
	while(x--);
}

/* Send 1 byte via MIN_UART Port */
void MinHwSerial_SendByte(uint8_t byte)
{
	HAL_UART_Transmit(&MIN_UART_HANDLER, &byte, 1, 10);
}

/* Send 1 byte via MIN_UART Port */
void MinHwSerial_SendBuffer(uint8_t* buffer, uint32_t lenth)
{
	HAL_UART_Transmit(&MIN_UART_HANDLER, buffer, lenth, 10);
}
