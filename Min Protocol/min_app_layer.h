/*
 *	Author: Huy TV7
 */
#ifndef MIN_APP_LAYER_H
#define MIN_APP_LAYER_H

#include <stdint.h>
#include "min_hw_serial.h"


#define MIN_ID_PING								(0x01U)			/* Layer 1 frame; Ping test: returns the same frame back */
#define MIN_ID_RESET							(0x05U)
#define MIN_ID_ERROR							(0x20u)

#define MIN_ID_REGISTER_WRITE			(0x30U)

#define MIN_ID_REGISTER_READ			(0x40u)
#define MIN_ID_REGISTER_REPORT		(0x41u)


#define MIN_ID_STM32_OTA_BEGIN								(0x50u)
#define MIN_ID_STM32_OTA_END									(0x51u)
#define MIN_ID_STM32_OTA_TRANSFER 						(0x52u)
#define MIN_ID_STM32_OTA_TRANSFER_CONFIRM 		(0x53u)
#define MIN_ID_STM32_VERSION					 				(0x60u)

uint16_t min_tx_space(uint8_t port);
void min_tx_byte(uint8_t port, uint8_t byte);
uint32_t min_time_ms(void);
void min_tx_start(uint8_t port);
void min_tx_finished(uint8_t port);
void min_application_handler(uint8_t min_id, uint8_t *min_payload, uint8_t len_payload, uint8_t port);
void do_min_reset(uint8_t id);
void do_register_report(uint8_t id, uint16_t addr, uint16_t length, uint16_t buf []);
void do_register_read(uint8_t m_id, uint8_t m_buf[], uint8_t m_lenth);
void do_register_write(uint8_t m_id, uint8_t m_buf[], uint8_t m_lenth);
void do_report_version(uint8_t id,uint16_t version);
void ping_handle(void);
void report_wifi_error(void);
static void do_ping(uint8_t m_id,uint8_t buf[]);
static void send_min_ota_state(uint8_t id, uint32_t size);
static void do_min_ota_state(uint8_t m_id,uint8_t m_buf[], uint8_t m_lenth);
static void  do_min_ota_transfer(uint8_t m_id,uint8_t m_buf[], uint8_t m_lenth);

#endif
