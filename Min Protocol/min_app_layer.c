/*
 *	Author: Huy TV7
 */
#include "min_app_layer.h"
#include "serial_debug.h"
#include "min.h"
#include "flash.h"

volatile uint32_t receivedNumber = 0;
volatile uint8_t m_buf[1024];
volatile uint32_t f_buf[256];
volatile uint32_t value[2] = {0x00000003, 0x00000000};


// volatile uint32_t flashAddrPointer = APP_UPGRADE_ADDRESS;
volatile uint32_t flashAddrPointer = 0x08020000;
static void do_min_ota_state(uint8_t min_id, uint8_t min_buf[], uint8_t min_lenth)
{
  // min_buf is uint16_t that have file size  with length = 4 byte ( unsigned long)
	uint8_t status;
	//vTaskDelay(100);
	if (min_id == MIN_ID_STM32_OTA_BEGIN) 
	{
		/* Erase flash*/
		// status = eraseFlash(APP_UPGRADE_ADDRESS, APP_SIZE);
		status = eraseFlash(flashAddrPointer, APP_SIZE);
		status += eraseFlash(CONFIGURATION_ADDRESS, CONFIGURATION_SIZE);
		/* Erase configuration area */
		if (status == FLASHIF_OK)
		{
				DEBUG_PRINTF("Flash was erased\r\n");
			  send_min_ota_state(MIN_ID_STM32_OTA_BEGIN, min_lenth);
		}
		else
		{
			DEBUG_PRINTF("Erase flash error\r\n");
		}
	} 
	else if(min_id == MIN_ID_STM32_OTA_END) 
	{
			status = writeFlash(CONFIGURATION_ADDRESS, (uint32_t*)value, 2);
			if (status == FLASHIF_OK) 
			{
					send_min_ota_state(MIN_ID_STM32_OTA_END, min_lenth);
					DEBUG_PRINTF("System reset\r\n");
					NVIC_SystemReset();					
			}
			{
				DEBUG_PRINTF("Write flash error\r\n");
			}
	}
}

static void  do_min_ota_transfer(uint8_t m_id, uint8_t m_buf[], uint8_t m_lenth)
{
	uint16_t i;
	uint32_t lengthconfirm;
	uint8_t  status;

	
	lengthconfirm = m_lenth;
	receivedNumber += m_lenth;			/*for debug ???????????*/
	
	//length of file need to write
	for(i = 0; i < m_lenth/4; i++)
	{
		f_buf[i] =  (((uint32_t)m_buf[4*i + 0]) << 0)   +   (((uint32_t)m_buf[4*i + 1]) << 8)   +  (((uint32_t)m_buf[4*i + 2]) << 8) + (((uint32_t)m_buf[4*i + 3]) << 24);
	}
	
	//__disable_irq();
	status = writeFlash(flashAddrPointer,(uint32_t*)f_buf, m_lenth/4);
	//__enable_irq();
	if (status == FLASHIF_OK) 
	{
		flashAddrPointer += m_lenth;
		send_min_ota_state(MIN_ID_STM32_OTA_TRANSFER_CONFIRM, lengthconfirm);
	}
}


uint16_t min_tx_space(uint8_t port)
{
  return 0xFFFF;
}

// Send a character on the designated port.
void min_tx_byte(uint8_t port, uint8_t byte)
{
	MinHwSerial_SendByte(byte);
}


void min_tx_start(uint8_t port)
{
	
}

void min_tx_finished(uint8_t port)
{
	
}

static void send_min_ota_state(uint8_t id, uint32_t size)
{
	
}


uint8_t test_data[128];
uint8_t test_id = 0;
volatile uint32_t test_count = 0;
void min_application_handler(uint8_t min_id, uint8_t *min_payload, uint8_t len_payload, uint8_t port)
{
// MIN_DBG_PRINTF("Frame was received, jump to min application\r\n");
//	test_id = min_id;
//	for(uint16_t i =0; i < len_payload; i++)
//	{
//		test_data[i] = *(min_payload + i);
//	}
	
		switch(min_id) 
		{
			case MIN_ID_PING:
					// do_ping(id,buf);
					MIN_DBG_PRINTF("IoT device ping\r\n");
			break;
			
			case MIN_ID_REGISTER_READ:
					MIN_DBG_PRINTF("IoT Device request to read Register\r\n");
					//do_register_read(min_id, buf, control);
			break;
			
			case MIN_ID_REGISTER_WRITE:
					MIN_DBG_PRINTF("IoT Device request to read Register\r\n");
					//do_register_write(id,buf,control);
			break;
			
			case MIN_ID_STM32_OTA_BEGIN:
					MIN_DBG_PRINTF("MIN OTA Begin\r\n");
					do_min_ota_state(min_id, min_payload, len_payload);
			break;
			
			case MIN_ID_STM32_OTA_END:
			{
					DEBUG_PRINTF("Number of byte received: %08x\r\n", test_count);
					MIN_DBG_PRINTF("MIN OTA End\r\n");
					uint32_t test = 0x00000003;
					writeFlash(CONFIGURATION_ADDRESS, &test, 1);
					do_min_ota_state(min_id, min_payload, len_payload);
				//eceivedNumber = 0;
			}
			break;
		
			case MIN_ID_STM32_OTA_TRANSFER:
					test_count++;
					do_min_ota_transfer(min_id, min_payload, len_payload);
			break;
		}
}
