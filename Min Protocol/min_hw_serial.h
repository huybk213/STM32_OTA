/*
*	Author: HuyTV7
 */
#ifndef MIN_HW_SERIAL_H
#define MIN_HW_SERIAL_H

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_uart.h"
#include "stm32l4xx_hal_rcc.h"
#include "stm32l4xx_hal_gpio.h"

#include "min_app_layer.h"
#include "global_var.h"

#define MIN_UART_HANDLER							huart4
#define MIN_UART_PORT									UART4
#define MIN_HW_SERIAL_RCC							RCC_PERIPHCLK_UART4
#define MIN_HW_SERIAL_CLK_SOURCE			RCC_UART4CLKSOURCE_PCLK1
#define MIN_HW_SERIAL_BAUD						9600

#define __MIN_HW_SERIAL_CLK_ENABLE() 		__HAL_RCC_UART4_CLK_ENABLE()
#define MIN_HW_SERIAL_TX_PIN					GPIO_PIN_0
#define MIN_HW_SERIAL_RX_PIN					GPIO_PIN_1
#define MIN_HW_SERIAL_AF							GPIO_AF8_UART4
#define MIN_HW_SERIAL_GPIO						GPIOA

#define MIN_HW_SERIAL_IRQn						UART4_IRQn
#define __MIN_HW_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOA_CLK_ENABLE()

void MinHwSerial_Init(void);
void MinHwSerial_SendByte(uint8_t byte);
void MinHwSerial_SendBuffer(uint8_t* buffer, uint32_t lenth);

#endif

