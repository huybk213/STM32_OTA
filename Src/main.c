/*
 * Author: HuyTV7
 */

#include "main.h"
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "min_hw_serial.h"
#include "serial_debug.h"
#include "string.h"
#include "global_var.h"

#include "min.h"
#include "min_app_layer.h"

#include "flash.h"

#include "led.h"

osThreadId defaultTaskHandle;
osThreadId minTaskHandle;

void SystemClock_Config(void);

void StartDefaultTask(void const * argument);
void StartMinTask(void const * argument);
uint8_t tmp;

struct min_context min_ctx;

SER_RING_BUF_T Ser_Debug_Buf;
uint8_t debug_in_buf[1024];

typedef  void (*pFunction)(void);
uint32_t JumpAddress1;
pFunction JumpToApplication1;
uint32_t test[2] = {0, 0};
uint32_t y = 0;
int main(void)
{
  SystemClock_Config();
	
  led_init();
	volatile uint32_t i = 0x000FFFFF;
	for(uint8_t ii = 0; ii < 10; ii++)
	{
		LED_TOGGLE();
		while(i--);
		i = 0x000FFFFF;
		
	}
	Debug_Init();
	MinHwSerial_Init();
	min_init_context(&min_ctx, 0);
	min_send_frame(&min_ctx, MIN_ID_STM32_OTA_BEGIN, (uint8_t*)"", 0); 
	min_send_frame(&min_ctx, MIN_ID_STM32_OTA_END, (uint8_t*)"", 0); 
	DEBUG_PRINTF("Starting OS ver %08x\r\n", 255);
	uint32_t status;
	status = eraseFlash(0x08020000, APP_SIZE);
	status += eraseFlash(CONFIGURATION_ADDRESS, CONFIGURATION_SIZE);
		/* Erase configuration area */
	if (status == FLASHIF_OK)
	{
			DEBUG_PRINTF("Flash was erased\r\n");
			//send_min_ota_state(MIN_ID_STM32_OTA_BEGIN, min_lenth);
	}
	uint32_t x[2] = {0x00000003, 0x00000000};
	
	if(	writeFlash(CONFIGURATION_ADDRESS, x, 2) != FLASHIF_OK)
	{
		DEBUG_PRINTF("Write error\r\n");
	}
	else
	{
		DEBUG_PRINTF("Write flash ok\r\n");
		readFlash(CONFIGURATION_ADDRESS, &y, 1);
	}
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
////	
	osThreadDef(minTask, StartMinTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(minTask), NULL);
  osKernelStart();
	//readFlash(0x080DF5B0, test, 2);
  while (1)
  {
//		if (((*(__IO uint32_t*)0x08008000) & 0x2FFE0000 ) == 0x20000000)
//    {
//      /* Jump to user application */
//      JumpAddress1 = *(__IO uint32_t*) (0x08008000 + 4);
//      JumpToApplication1 = (pFunction) JumpAddress1;
//      /* Initialize user application's Stack Pointer */
//      __set_MSP(*(__IO uint32_t*) 0x08008000);
//      JumpToApplication1();
//    }
  }
}

void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
	
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
	
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}



void StartDefaultTask(void const * argument)
{
	//DEBUG_PRINTF("StartDefaultTask\r\n");
  for(;;)
  {
		 DEBUG_PRINTF(".");
//		uint8_t x[2] = {0xAA, 0xAA};
//		min_send_frame(&min_ctx,  20 , x, 2);
		osDelay(1000);
  }
}

volatile uint8_t data_min;
extern	SER_RING_BUF_T Ser_Min_Buf;
static void StartMinTask(void const * argument)
{
	
	for(;;)
  {
		//DEBUG_PRINTF((const char*)"StartMinTask\r\n", strlen((const char*)"StartMinTask\r\n"));
		if(!SER_RING_BUF_EMPTY(Ser_Min_Buf))
		{
			data_min = SER_RING_BUF_RD(Ser_Min_Buf);
			min_rx_byte(&min_ctx, data_min);
		}
		osDelay(1);
  }
}

//static void StartTestIap(void const * argument)
//{
//	
//	for(;;)
//  {
//		//DEBUG_PRINTF((const char*)"StartMinTask\r\n", strlen((const char*)"StartMinTask\r\n"));
//		if(!SER_RING_BUF_EMPTY(Ser_Debug_Buf))
//		{
//			//writeFlash(APPLICATION_ADDRESS, 
//			//data_min = SER_RING_BUF_RD(Ser_Debug_Buf);
//			//min_rx_byte(&min_ctx, data_min);
//		}
//		osDelay(1);
//  }
//}




void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{

  if (htim->Instance == TIM2) 
	{
    HAL_IncTick();
  }

}

void _Error_Handler(char *file, int line)
{

  while(1)
  {

  }
}

