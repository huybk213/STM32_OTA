

#include "stm32l4xx_hal.h"
#include "stm32l4xx.h"
#include "stm32l4xx_it.h"
#include "cmsis_os.h"
#include "global_var.h"
#include "min.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern TIM_HandleTypeDef htim2;


void NMI_Handler(void)
{
	
}


void HardFault_Handler(void)
{
  while (1)
  {
		HAL_UART_Transmit(&DEBUG_UART_HANDLER, "Hardfault\r\n", 11, 1000);
		while(1);
  }
}


void MemManage_Handler(void)
{
  while (1)
  {
		
  }
}


void BusFault_Handler(void)
{
  while (1)
  {
		
  }
}


void UsageFault_Handler(void)
{
  while (1)
  {
		
  }
}


void DebugMon_Handler(void)
{

}


void SysTick_Handler(void)
{
  osSystickHandler();
}




void TIM2_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim2);
}

/* Debug serial	*/
extern SER_RING_BUF_T Ser_Debug_Buf;
void USART2_IRQHandler(void)
{
	uint8_t byte  = USART2->RDR;
	if(!SER_RING_BUF_FULL(Ser_Debug_Buf))				SER_RING_BUF_WR(Ser_Debug_Buf, byte);
}

volatile uint32_t count = 0;
volatile uint32_t over_flow = 0;
extern	SER_RING_BUF_T Ser_Min_Buf;
/* MIN serial handler	*/
void UART4_IRQHandler(void)
{
	uint8_t byte;
	UART4->ICR |= 2;
	if(UART4->ISR& 0x00000020)
	{
			byte = UART4->RDR;
			count++;
			if(!SER_RING_BUF_FULL(Ser_Min_Buf))
			{		
				count++;
				SER_RING_BUF_WR(Ser_Min_Buf, byte);
			}
			else 
			{
				count  = 0;
				over_flow++;
				SER_RING_BUF_RESET(Ser_Min_Buf);
			}
	}
	
	
	
}
