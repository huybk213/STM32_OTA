/*
 * Author: HuyTV7
 */
#ifndef	SERIAL_DEBUG_H 
#define SERIAL_DEBUG_H

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>

#define DEBUG_ENABLE			1
#if	DEBUG_ENABLE
	#define DEBUG_PRINTF(...) 	printf(__VA_ARGS__)
	#elif DEBUG_ENABLE == 0
	#define DEBUG_PRINTF(...)		while(0)
#endif

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_uart.h"
#include "stm32l4xx_hal_gpio.h"
#include "stm32l4xx_hal_rcc.h"
	
//#define DEBUG_UART_HANDLER							huart1
//#define DEBUG_UART_PORT									USART1
//#define DEBUG_HW_SERIAL_RCC							RCC_PERIPHCLK_USART1
//#define DEBUG_HW_SERIAL_CLK_SOURCE			RCC_USART1CLKSOURCE_PCLK2
//#define DEBUG_HW_SERIAL_BAUD						460800

//#define DEBUG_HW_SERIAL_CLK_ENABLE() 		__HAL_RCC_USART1_CLK_ENABLE()
//#define DEBUG_HW_SERIAL_TX_PIN					GPIO_PIN_9
//#define DEBUG_HW_SERIAL_RX_PIN					GPIO_PIN_10
//#define DEBUG_HW_SERIAL_AF							GPIO_AF7_USART1
//#define DEBUG_HW_SERIAL_GPIO						GPIOA

//#define DEBUG_HW_SERIAL_IRQn						USART1_IRQn	
//#define __DEBUG_HW_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOA_CLK_ENABLE();


#define DEBUG_UART_HANDLER							huart2
#define DEBUG_UART_PORT									USART2
#define DEBUG_HW_SERIAL_RCC							RCC_PERIPHCLK_USART2
#define DEBUG_HW_SERIAL_CLK_SOURCE			RCC_USART2CLKSOURCE_PCLK1
#define DEBUG_HW_SERIAL_BAUD						115200

#define DEBUG_HW_SERIAL_CLK_ENABLE() 		__HAL_RCC_USART2_CLK_ENABLE()
#define DEBUG_HW_SERIAL_TX_PIN					GPIO_PIN_5
#define DEBUG_HW_SERIAL_RX_PIN					GPIO_PIN_6
#define DEBUG_HW_SERIAL_AF							GPIO_AF7_USART2
#define DEBUG_HW_SERIAL_GPIO						GPIOD

#define DEBUG_HW_SERIAL_IRQn						USART2_IRQn

#define __DEBUG_HW_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOD_CLK_ENABLE();


/* Debug serial init function */
void Debug_Init(void);
	
#endif
