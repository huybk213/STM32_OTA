/*
 *	Author: HuyTV7
 */
 
#include "serial_debug.h"

UART_HandleTypeDef DEBUG_HANDLER;

/* Retarget */
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int byte, FILE *f)
#define GETCHAR_PROTOTYPE int fgetc(FILE *f)
#endif /* __GNUC__ */

UART_HandleTypeDef DEBUG_UART_HANDLER;

/* Debug serial init function */
void Debug_Init(void)
{
	/* Init clock */
	RCC_PeriphCLKInitTypeDef PeriphClkInit;
	PeriphClkInit.PeriphClockSelection = DEBUG_HW_SERIAL_RCC;
  PeriphClkInit.Usart2ClockSelection = DEBUG_HW_SERIAL_CLK_SOURCE;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
	
	/* MSP Init*/
	GPIO_InitTypeDef GPIO_InitStruct;
  DEBUG_HW_SERIAL_CLK_ENABLE();
	
  __DEBUG_HW_GPIO_CLK_ENABLE();
  
	/*	USART GPIO Configuration */   
  GPIO_InitStruct.Pin = DEBUG_HW_SERIAL_TX_PIN|DEBUG_HW_SERIAL_RX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = DEBUG_HW_SERIAL_AF;
  HAL_GPIO_Init(DEBUG_HW_SERIAL_GPIO, &GPIO_InitStruct);

  /* USART interrupt Init */
  HAL_NVIC_SetPriority(DEBUG_HW_SERIAL_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DEBUG_HW_SERIAL_IRQn);

	/*Config UART	*/
	DEBUG_UART_HANDLER.Instance = DEBUG_UART_PORT;
  DEBUG_UART_HANDLER.Init.BaudRate = DEBUG_HW_SERIAL_BAUD;
  DEBUG_UART_HANDLER.Init.WordLength = UART_WORDLENGTH_8B;
  DEBUG_UART_HANDLER.Init.StopBits = UART_STOPBITS_1;
  DEBUG_UART_HANDLER.Init.Parity = UART_PARITY_NONE;
  DEBUG_UART_HANDLER.Init.Mode = UART_MODE_TX_RX;
  DEBUG_UART_HANDLER.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  DEBUG_UART_HANDLER.Init.OverSampling = UART_OVERSAMPLING_16;
  DEBUG_UART_HANDLER.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  DEBUG_UART_HANDLER.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  HAL_UART_Init(&DEBUG_UART_HANDLER);
	uint8_t tmp;
	HAL_UART_Receive_IT(&DEBUG_HANDLER, &tmp, 1);
}

PUTCHAR_PROTOTYPE
{
  HAL_UART_Transmit(&DEBUG_UART_HANDLER, (uint8_t*)&byte, 1, 0xFF);
  return byte;
}

