/**
 ******************************************************************************
 * Author: HuyTV7
 *
 * MEMORY Distribution
 * 	---------------------	0x08000000	(128KB)
 *	| BootLoader		    |			
 *	|					|
 *	|-------------------|	0x08020000	(64KB)
 *	|					|
 *	| APPLICATION		|		 	
 *	|					|
 *	|-------------------|	0x08030000	(64KB)
 *	|					|
 *	| APP UPGRADE		|		 
 *	|					|
 *	---------------------	0x08040000	(64KB)
 *	|					|
 *	| UNBRICK FIRMWARE	|
 *	|					|
 *	|-------------------|	0x08050000	(2KB)
 *  |                   |
 *  | Configuration     |
 *  |-------------------|	0x08050800	(702KB)
 *	| RESERVED			|
 *	---------------------	0x08100000
 */
 
/*
 * Author: HuyTV7
 */

#ifndef _IAP_H
#define _IAP_H

/* Include -------------------------------------------------------------------- */
#include "stm32l4xx_hal.h"

/* Type Definition -------------------------------------------------------------*/
typedef  void (*pFunction)(void);

enum 
{
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
};

typedef enum 
{
	IAP_OK = 0,
	IAP_NOFIRMWARE_ERROR,
	IAP_ERASEFLASH_ERROR,
	IAP_WRITEFLASH_ERROR
} IAP_Status;

/* Parameters Definition ------------------------------------------------------*/
#define FLASH_START_ADRESS						(uint32_t)0x08000000
#define USER_FLASH_END_ADDRESS				(uint32_t)0x08100000
#define FLASH_PAGE_NBPERBANK					 256

#define APPLICATION_ADDRESS 					(uint32_t)0x08020000
#define APP_UPGRADE_ADDRESS						(uint32_t)0x08030000
#define APP_UNBRICK_ADDRESS						(uint32_t)0x08040000
#define CONFIGURATION_ADDRESS 				(uint32_t)0x08050000
#define APP_SIZE											(uint32_t)0x10000
#define CONFIGURATION_SIZE						(uint32_t)0x0800

void initIAP(void);
static uint32_t writeFlash(uint32_t destination, uint32_t *p_source, uint32_t length);
static uint32_t eraseFlash(uint32_t startAddress, uint32_t size);
IAP_Status runApplication(void);
IAP_Status unbrickBoard(void);
IAP_Status upgradeFirmware(void);

#endif /* _IAP_H */
