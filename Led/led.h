/*
 * Author: HuyTV
 */
 
#ifndef LED_H
#define LED_H
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_rcc.h"
#include "stm32l4xx_hal_gpio.h"


#define GPIOLED														GPIOE
#define __HAL_RCC_GPIOLED_CLK_ENABLE()		__HAL_RCC_GPIOE_CLK_ENABLE()
#define LED_PIN														GPIO_PIN_8
#define LED_ON()													HAL_GPIO_WritePin(GPIOLED, LED_PIN)													
#define LED_OFF()													HAL_GPIO_ReadPin(GPIOLED, LED_PIN)				
#define LED_TOGGLE()											HAL_GPIO_TogglePin(GPIOLED, LED_PIN)

void led_init(void);


#endif
