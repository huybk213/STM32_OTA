/*
 * Author: HuyTV
 */

#include "flash.h"

void initFlash(void) 
{
  /* Unlock the Program memory */
  HAL_FLASH_Unlock();
  /* Clear all FLASH flags */
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGSERR | FLASH_FLAG_WRPERR | FLASH_FLAG_OPTVERR);
  /* Unlock the Program memory */
  HAL_FLASH_Lock();
}


uint32_t eraseFlash(uint32_t startAddress, uint32_t size) 
{
	uint32_t StartPage = 0;
	uint32_t EndPage = 511;
	uint32_t PageError = 0;

	FLASH_EraseInitTypeDef pEraseInit;
	HAL_StatusTypeDef status = HAL_OK;

	/* Unlock the Flash to enable the flash control register access *************/ 
	HAL_FLASH_Unlock();

	/* Start Page to erase*/
	StartPage = (startAddress - FLASH_START_ADRESS) / FLASH_PAGE_SIZE;
	
	/* End Page to erase */
	EndPage = StartPage + size/FLASH_PAGE_SIZE - 1;
	
	if ((StartPage < FLASH_PAGE_NBPERBANK) && (EndPage < FLASH_PAGE_NBPERBANK)) 
	{
		pEraseInit.Banks = FLASH_BANK_1;
		pEraseInit.NbPages = EndPage - StartPage + 1;
		pEraseInit.Page = StartPage;
		pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
		status = HAL_FLASHEx_Erase(&pEraseInit, &PageError);
	}
	else if ((StartPage > FLASH_PAGE_NBPERBANK) && (EndPage > FLASH_PAGE_NBPERBANK)) 
	{
		pEraseInit.Banks = FLASH_BANK_2;
		pEraseInit.NbPages = EndPage - StartPage + 1;
		pEraseInit.Page = StartPage;
		pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
		status = HAL_FLASHEx_Erase(&pEraseInit, &PageError);
	} 
	else 
	{
		pEraseInit.Banks = FLASH_BANK_1;
		pEraseInit.NbPages = FLASH_PAGE_NBPERBANK - StartPage - 1;
		pEraseInit.Page = StartPage;
		pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
		status = HAL_FLASHEx_Erase(&pEraseInit, &PageError);
		
		if (status == HAL_OK) 
		{
			pEraseInit.Banks = FLASH_BANK_2;
			pEraseInit.NbPages = EndPage - FLASH_PAGE_NBPERBANK + 1;
			pEraseInit.Page = FLASH_PAGE_NBPERBANK;
			pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
			status = HAL_FLASHEx_Erase(&pEraseInit, &PageError);
		}
	}

	/* Lock the Flash to disable the flash control register access (recommended
	 to protect the FLASH memory against possible unwanted operation) *********/
	HAL_FLASH_Lock();

	if (status != HAL_OK)
	{
	/* Error occurred while page erase */
	return FLASHIF_ERASEKO;
	}

	return FLASHIF_OK;
}
/**
  * @brief  This function writes a data buffer in flash (data are 32-bit aligned).
  * @note   After writing data buffer, the flash content is checked.
  * @param  destination: start address for target location
  * @param  p_source: pointer on buffer with data to write
  * @param  length: length of data buffer (unit is 32-bit word)
  * @retval uint32_t 0: Data successfully written to Flash memory
  *         1: Error occurred while writing data in Flash memory
  *         2: Written Data in flash memory is different from expected one
  */
uint32_t writeFlash(uint32_t destination, uint32_t *p_source, uint32_t length)
{
  uint32_t status = FLASHIF_OK;
  uint32_t i = 0;

  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  /* DataLength must be a multiple of 64 bit */
  for (i = 0; (i < length/2) && (destination <= (USER_FLASH_END_ADDRESS-8)); i++)
  {
    /* Device voltage range supposed to be [2.7V to 3.6V], the operation will
       be done by word */ 
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, destination, *((uint64_t *)(p_source+2*i))) == HAL_OK)      
    {
     /* Check the written value */
      if (*(uint64_t*)destination != *(uint64_t *)(p_source+2*i))
      {
        /* Flash content doesn't match SRAM content */
        status = FLASHIF_WRITINGCTRL_ERROR;
        break;
      }
      /* Increment FLASH destination address */
      destination += 8;
    }
    else
    {
      /* Error occurred while writing data in Flash memory */
      status = FLASHIF_WRITING_ERROR;
      break;
    }
  }

  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();

  return status;
}

/**
  * @brief  This function reads a data buffer in flash (data are 32-bit aligned).
  * @param  destination: start address for target location
  * @param  p_source: pointer on buffer with data to read
  * @param  length: length of data buffer (unit is 32-bit word)
  * @retval none
  */
void readFlash(uint32_t destination, uint32_t* buffer, uint32_t lenth)
{
	while(lenth--)
	{
	 *buffer = (uint32_t)(*((__IO uint32_t*)(destination)));
		buffer++;
		destination += 4;
	}
}

