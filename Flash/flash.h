/*
 * Author: HuyTV
 */
 
#ifndef FLASH_H
#define FLASH_H

#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

enum 
{
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
};

/* Parameters Definition ------------------------------------------------------*/
#define FLASH_START_ADRESS						(uint32_t)0x08000000
#define USER_FLASH_END_ADDRESS				(uint32_t)0x08100000
#define FLASH_PAGE_NBPERBANK					 256

#define APPLICATION_ADDRESS 					(uint32_t)0x08008000
#define APP_UPGRADE_ADDRESS						(uint32_t)0x08010000	
#define APP_UNBRICK_ADDRESS						(uint32_t)0x08018000
#define CONFIGURATION_ADDRESS 				(uint32_t)0x08030000
#define APP_SIZE											(uint32_t)0x8000
#define CONFIGURATION_SIZE						(uint32_t)0x0800

void initFlash(void);
extern uint32_t writeFlash(uint32_t destination, uint32_t *p_source, uint32_t length);
extern uint32_t eraseFlash(uint32_t startAddress, uint32_t size);
extern void readFlash(uint32_t destination, uint32_t* buffer, uint32_t lenth);
#endif
